angular.module('Gobble', ['ionic'])

.run(function($ionicPlatform) {
  $ionicPlatform.ready(function() {
    // Hide the accessory bar by default (remove this to show the accessory bar above the keyboard
    // for form inputs)
    if (window.cordova && window.cordova.plugins && window.cordova.plugins.Keyboard) {
      cordova.plugins.Keyboard.hideKeyboardAccessoryBar(true);
      cordova.plugins.Keyboard.disableScroll(true);

    }
    if (window.StatusBar) {
      // org.apache.cordova.statusbar required
      StatusBar.styleDefault();
    }
  });
})

.config(function($stateProvider, $urlRouterProvider) {

  $stateProvider

  .state('about-us', {
    url: '/about-us',
    templateUrl: 'modules/about-us/aboutUs.view.html'
  })

  // STATIC CONTENT VIEWS

  .state('home', {
    url: '/home',
    templateUrl: 'modules/home/home.view.html'
  })

  .state('recipe', {
    url: '/recipe',
    templateUrl: 'modules/fridge/views/recipeList/views/recipe.view.html'
  })

  .state('fridge-item', {
    url: '/fridge-item', // TODO: Add the routing parameters here      //url: '/fridge-item:iD',
    templateUrl: 'modules/fridge/views/fridgeList/views/fridgeItem.view.html'
  })

  .state('fridge', {
    url: '/fridge',
    abstract: true,
    templateUrl: 'modules/fridge/fridge.view.html'
  })

  .state('fridge.stats', {
    url: '/stats',
    views: {
      'fridge-stats': {
        templateUrl: 'modules/fridge/views/stats/stats.view.html',
        controller: 'StatsController'
      }
    }
  })

  .state('fridge.fridgeList', {
    url: '/fridgeList',
    views: {
      'fridge-fridgeList': {
        templateUrl: 'modules/fridge/views/fridgeList/fridgeList.view.html',
        controller: 'FridgeListController'
      }
    }
  })

  .state('fridge.recommendations', {
    url: '/recommendations',
    views: {
      'fridge-recommendations': {
        templateUrl: 'modules/fridge/views/recommendations/recommendations.view.html',
        controller: 'RecommendationsController'
      }
    }
  })

  .state('fridge.recipeList', {
    url: '/recipeList',
    views: {
      'fridge-recipeList': {
        templateUrl: 'modules/fridge/views/recipeList/recipeList.view.html',
        controller: 'RecipeListController'
      }
    }
  })

  .state('login', {
      url: '/login',
      templateUrl: 'modules/login/login.view.html',
      controller: 'LoginController'
  })

  .state('preferences', {
      url: '/preferences',
      templateUrl: 'modules/preferences/preferences.view.html'
  })

  .state('product-info', {
      url: '/product-info', // TODO: Add the routing parameters here      //url: '/product-info:pID',
      templateUrl: 'modules/productInfo/productInfo.view.html',
      controller: 'ProductInfo'
  })

  .state('profile', {
      url: '/profile',
      templateUrl: 'modules/profile/profile.view.html',
      controller:'ProfileController'
  })

  .state('signup', {
      url: '/signup',
      templateUrl: 'modules/signup/userForm.view.html'
  })

  .state('store', {
    url: '/store',
    abstract: true,
    templateUrl: 'modules/store/store.view.html'
  })

  .state('store.carryovers', {
    url: '/carryovers',
    views: {
      'store-carryovers': {
        templateUrl: 'modules/store/views/carryovers/carryovers.view.html',
        controller: 'CarryoversController'
      }
    }
  })

  .state('store.shoppingList', {
    url: '/shoppingList',
    views: {
      'store-shoppingList': {
        templateUrl: 'modules/store/views/shoppingList/shoppingList.view.html',
        controller: 'ShoppingListController'
      }
    }
  })

  .state('store.shoppingCart', {
    url: '/shoppingCart',
    views: {
      'store-shoppingCart': {
        templateUrl: 'modules/store/views/shoppingCart/shoppingCart.view.html',
        controller: 'ShoppingCartController'
      }
    }
  })

  .state('shoppingListAdd', {
    url: '/shoppingListAdd',
    templateUrl: 'modules/store/views/shoppingList/shoppingListAdd/shoppingListAdd.view.html',
    controller: 'ShoppingListAddController'
  });


  // if none of the above states are matched, use this as the fallback
  $urlRouterProvider.otherwise('/home');

});
