//--> Sample for app.environment.js

// THIS IS A SAMPLE FILE //

/* Instructions
 *
 * Rename this file to app.environment.js
 *
 * Change local_dev environment urls to your local set up
 *
 * You're local dev will be the urls to connect you to your local dev environment including your local db setup
 *
 * Please keep in mind that live_dev is a LIVE development server and we don't really want to be fucking it up.
 *
 * staging_dev is our https dev server which is purely for staging purposes to make sure everything is working
 * on our secure server
 *
 */


angular.module('Gobble').run(['$rootScope', function($rootScope) {
  $rootScope.environment = 'local_dev';
  $rootScope.user = {};
  $rootScope.user.preferences = {};
  $rootScope.user.preferences.isAllegNuts = "FALSE";


  if ($rootScope.environment == 'local_dev') {
    $rootScope.apiUrl = 'http://localhost/~Sach/Gobble-API/api/';
    $rootScope.appUrl = 'http://localhost:8100/#/';

  } else if ($rootScope.environment == 'staging_dev') {
    $rootScope.apiUrl = 'http://devapi.hicarbyecar.com/';
    $rootScope.appUrl = 'http://dev.hicarbyecar.com/#/';

  }

  else if ($rootScope.environment == 'asdfkajfsdgs') {}

}]);
