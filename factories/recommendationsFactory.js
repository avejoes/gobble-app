(function() {
  var recommendationsFactory = function($rootScope, $http) {

    var uploadFactory = {};

    recommendationsFactory.getRecomendations = function(dealerId) {
      return $http({
        method: 'post',
        dataType: 'json',
        data: {
          //id: dealerId
        },
        url: $rootScope.apiUrl + 'data/admin/getOneDealerInfo.php',
        header: {
          'Content-Type': 'application/json'
        }
      });
    }

    return recommendationsFactory;
  }

  recommendationsFactory.$inject = ['$rootScope', '$http'];

  angular.module('Gobble').factory('recommendationsFactory', recommendationsFactory);

}());
