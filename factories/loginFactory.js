(function() {
  var loginFactory = function($rootScope, $http) {

    loginFactory.login = function(usr, pass) {
      return $http({
        method: 'post',
        dataType: 'json',
        data: {
          username: usr,
          password: pass
        },
        url: 'http://localhost/~Sach/Gobble-API/api/data/shared/login/login.php',
        header: {
          'Content-Type': 'application/json'
        }
      });
    }
    return loginFactory;
  }

  loginFactory.$inject = ['$rootScope', '$http'];

  angular.module('Gobble').factory('loginFactory', loginFactory);

}());
