(function() {
  var productInfoSearchFactory = function($rootScope, $http) {

    productInfoSearchFactory.getData = function(key) {
      return $http({
        method: 'post',
        dataType: 'json',
        data: {
          keyword: key
        },
        url: 'http://localhost/~Sach/Gobble-API/api/data/shared/login/login.php',
        header: {
          'Content-Type': 'application/json'
        }
      });
    }
    return productInfoSearchFactory;
  }

  productInfoSearchFactory.$inject = ['$rootScope', '$http'];

  angular.module('Gobble').factory('productInfoSearchFactory', productInfoSearchFactory);

}());
