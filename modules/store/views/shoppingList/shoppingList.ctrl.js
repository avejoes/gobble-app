(function() {
  var ShoppingListController = function($scope, $ionicPopup, cartItem, listItem) {
    // TODO: get data from somewhere
    $scope.list = listItem.getList();

    $scope.remove = function(item) {
      listItem.remove(item);
    };

    $scope.cart = function(item) {
      cartItem.cart(item);
      $scope.remove(item);
    };

    // Camera alert
    $scope.showAlert = function() {
      var alertPopup = $ionicPopup.alert({
        title: 'Permission Denied.',
        template: 'This is a premium feature. You must purchase the full version to unlock.'
      });
    };
  };

  ShoppingListController.$inject = ['$scope', '$ionicPopup', 'cartItem', 'listItem'];

  angular.module('Gobble').controller('ShoppingListController', ShoppingListController)
})();

// Service to add items to the cart
angular.module('Gobble').service('listItem', function () {
  var list = ['Milk', 'Wine', 'Vodka', 'Salt'];

  return {
    list: function (item) {
      if(list.indexOf(item) < 0) {
        list.push(item);
      }
    },

    remove: function (item) {
      var index = list.indexOf(item);
      if(index >= 0){
        list.splice(index, 1);
      }
    },

    getList: function() {
      return list;
    }
  };
});
