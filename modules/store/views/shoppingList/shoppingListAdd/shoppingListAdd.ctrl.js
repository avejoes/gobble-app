(function() {
var ShoppingListAddController =function($scope, $state, listItem) {
  $scope.data = [
    {name: 'Milk', checked: false},
    {name: 'Bread', checked: false},
    {name: 'Chicken Livers', checked: false},
    {name: 'Koo Beans', checked: false}];

  var list = listItem.getList();
  for(i = 0; i < $scope.data.length; ++i){
    // If the item is in the shopping list, check it
    var item = $scope.data[i];
    if(list.indexOf(item.name) >= 0){
      item.checked = true;
    }
  }

  $scope.done = function(item) {
    for(i = 0; i < $scope.data.length; ++i){
      var item = $scope.data[i];
      if(item.checked){
        listItem.list(item.name);
      } else {
        listItem.remove(item.name);
      }
    }

    $state.go("store.shoppingList");
  }
};

ShoppingListAddController.$inject = ['$scope', '$state', 'listItem'];

angular.module('Gobble').controller('ShoppingListAddController', ShoppingListAddController)

})();
