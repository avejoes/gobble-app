(function() {
  var ShoppingCartController = function($scope, $ionicPopup, cartItem, fridgeItem) {
    // TODO: get data from somewhere
    $scope.cart = [];
    $scope.cart = cartItem.getCart();

    $scope.remove = function(item) {
      var index = $scope.cart.indexOf(item);
      $scope.cart.splice(index, 1);
    };

    $scope.checkout = function() {
      // Add all items to the fridge and empty the cart
      for(i = 0; i < $scope.cart.length; ++i){
        fridgeItem.fridge($scope.cart[i]);
      }
      $scope.cart = [];

      $scope.vitalityAlert();
    }

    // Camera alert
    $scope.showAlert = function() {
     var alertPopup = $ionicPopup.alert({
       title: 'Permission Denied.',
       template: 'This is a premium feature. You must purchase the full version to unlock.'
     });
    };

    // Vitality points alert
    $scope.vitalityAlert = function() {
     var alertPopup = $ionicPopup.alert({
       title: '+200 Vitality Points',
       template: 'You have been rewarded for eating healthy.'
     });
    };
  };

ShoppingCartController.$inject = ['$scope', '$ionicPopup', 'cartItem', 'fridgeItem'];

angular.module('Gobble').controller('ShoppingCartController', ShoppingCartController)

})();

// Service to add items to the cart
angular.module('Gobble').service('cartItem', function () {
  var cart = ["Milk", "Lamb Chops", "Pepper", "Salt"];

  return {
    cart: function (item) {
      if(cart.indexOf(item) < 0) {
        cart.push(item);
      }
    },

    getCart: function() {
      return cart;
    }
  };
});
