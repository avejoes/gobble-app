(function() {
var CarryoversController = function($scope) {
  // TODO: get data from database, specifically whats in the fridge
  $scope.fridge = [
    {name: 'Milk', checked: false},
    {name: 'Bread', checked: false},
    {name: 'Chicken Livers', checked: false},
    {name: 'Koo Beans', checked: false}];

  $scope.carry = function() {
    for(i = 0; i < $scope.fridge.length; ++i){
      item = $scope.fridge[i];
      // Carry over the checked items
      if(item.checked) {
        // TODO: send to fridge
      }
    }
  };
};

angular.module('Gobble').controller('CarryoversController', CarryoversController)

})();
