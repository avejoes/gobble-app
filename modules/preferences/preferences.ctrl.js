(function() {
var PreferencesController =function($scope) {

//Preferences
  $scope.isVegan = "true";
  $scope.isVegterian = "false";
  $scope.isHalaal = "true";
  $scope.isCosher = "false";
  $scope.isPescatarian = "false";
  $scope.isDiabetic = "false";

//Allergies
  $scope.isNuts = "true";
  $scope.isLactose = "false";
  $scope.isEggs = "true";
  $scope.isShellfish = "false";
  $scope.isGluten = "true";
  $scope.isSoy = "false";
};

//PreferencesController.$inject = [$scope];

angular.module('Gobble').controller('PreferencesController', PreferencesController)

})();
