(function() {
var RecipeListController =function($scope) {

  $scope.data = ["Recommendation Item 1", "Recommendation Item 2", "Recommendation Item 3", "Recommendation Item 4", "Recommendation Item 5"];


  $scope.recipeTitles = ["Stir Fried Kale"]
  $scope.reviews   //$scope.recipeStep1 = ["Rub the beets with 1 tablespoon of olive oil and place onto prepared baking sheet. Place in preheated oven, and bake until the beets can be easily pierced with a fork, 20 to 60 minutes depending on size. When done, allow to cool on the baking sheet until cool enough to handle, then remove and discard the skin, and cut the beets into wedges or slices.", "Heat olive oil in a saucepan over medium heat. Stir in quinoa and allow to toast for 2 to 3 minutes, then add chicken broth, soy sauce, ginger and garlic. Increase heat and bring to a boil. Cover and reduce heat to low. Simmer until all liquid has been absorbed, 25 to 30 minutes. Fluff quinoa with fork and top with green onions before serving.", "Cut zucchini into noodle-shaped strands using a spiralizing tool. Place zoodles in a large bowl and top with cucumber, tomatoes, olives, red onion, and feta cheese.","Heat oil over medium-high heat in a large frying pan. Add onions and garlic; cook and stir until soft."]

};

RecipeListController.$inject = ['$scope'];

angular.module('Gobble').controller('RecipeListController', RecipeListController)

})();
