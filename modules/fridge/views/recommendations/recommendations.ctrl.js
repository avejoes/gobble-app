(function() {
var RecommendationsController =function($scope) {


  $scope.omega3Foods = ["Salmon", "Anchovie", "Cod Liver Oil"]

  $scope.otherFoods = ["Spinach", "Cabbage", "Kale", "Chicken Livers", "Lentils", "Chick Peas"];
};

//RecommendationsController.$inject = [$scope];

angular.module('Gobble').controller('RecommendationsController', RecommendationsController)
})();
