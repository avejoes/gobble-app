(function() {
var StatsController =function($scope) {

  // Radar Chart Options
var radarOptions = {

	//Boolean - If we show the scale above the chart data
	scaleOverlay : false,

	//Boolean - If we want to override with a hard coded scale
	scaleOverride : false,

	//** Required if scaleOverride is true **
	//Number - The number of steps in a hard coded scale
	scaleSteps : null,
	//Number - The value jump in the hard coded scale
	scaleStepWidth : null,
	//Number - The centre starting value
	scaleStartValue : null,

	//Boolean - Whether to show lines for each scale point
	scaleShowLine : true,

	//String - Colour of the scale line
	scaleLineColor : "#999",

	//Number - Pixel width of the scale line
	scaleLineWidth : 1,

	//Boolean - Whether to show labels on the scale
	scaleShowLabels : false,

	//Interpolated JS string - can access value
	scaleLabel : "<%=value%>",

	//String - Scale label font declaration for the scale label
	scaleFontFamily : "'Arial'",

	//Number - Scale label font size in pixels
	scaleFontSize : 12,

	//String - Scale label font weight style
	scaleFontStyle : "normal",

	//String - Scale label font colour
	scaleFontColor : "#666",

	//Boolean - Show a backdrop to the scale label
	scaleShowLabelBackdrop : true,

	//String - The colour of the label backdrop
	scaleBackdropColor : "rgba(255,255,255,0.75)",

	//Number - The backdrop padding above & below the label in pixels
	scaleBackdropPaddingY : 2,

	//Number - The backdrop padding to the side of the label in pixels
	scaleBackdropPaddingX : 2,

	//Boolean - Whether we show the angle lines out of the radar
	angleShowLineOut : true,

	//String - Colour of the angle line
	angleLineColor : "rgba(255,255,255,0.3)",

	//Number - Pixel width of the angle line
	angleLineWidth : 1,

	//String - Point label font declaration
	pointLabelFontFamily : "'Arial'",

	//String - Point label font weight
	pointLabelFontStyle : "normal",

	//Number - Point label font size in pixels
	pointLabelFontSize : 12,

	//String - Point label font colour
	pointLabelFontColor : "#EFEFEF",

	//Boolean - Whether to show a dot for each point
	pointDot : true,

	//Number - Radius of each point dot in pixels
	pointDotRadius : 3,

	//Number - Pixel width of point dot stroke
	pointDotStrokeWidth : 1,

	//Boolean - Whether to show a stroke for datasets
	datasetStroke : true,

	//Number - Pixel width of dataset stroke
	datasetStrokeWidth : 1,

	//Boolean - Whether to fill the dataset with a colour
	datasetFill : true,

	//Boolean - Whether to animate the chart
	animation : true,

	//Number - Number of animation steps
	animationSteps : 30,

	//String - Animation easing effect
	animationEasing : "easeOutQuart",

	//Function - Fires when the animation is complete
	onAnimationComplete : null

}

//radar data
var radarData = {
	labels : ["Fats","Protein","Carbs","Excercise","Sleep","Alcohol","Water"],
	datasets : [
		{
			fillColor : "rgba(220,220,220,0.5)",
			strokeColor : "rgba(220,220,220,1)",
			data : [65,59,90,81,56,55,40]
		},
		{
			fillColor : "rgba(151,187,205,0.5)",
			strokeColor : "rgba(151,187,205,1)",
			data : [28,48,40,19,96,27,100]
		}
	]
}

//Get the context of the Radar Chart canvas element we want to select
var ctx3 = document.getElementById("radarChart").getContext("2d");

// Create the Radar Chart
var myRadarChart = new Chart(ctx3).Radar(radarData, radarOptions);







  var lineChartData = {
      labels: ["Friday", "Saturday", "Sunday", "Monday", "Tuesday"],
      datasets: [{
          fillColor: "rgba(220,220,220,0)",
          strokeColor: "rgba(0,63,136,1)",
          pointColor: "rgba(220,180,0,1)",
          data: [80, 20, 40, 10, 60]
      }]
  }

  Chart.defaults.global.animationSteps = 100;
  Chart.defaults.global.animationEasing = "easeOutBounce";
  //Chart.defaults.global.responsive = true;

  var ctx = document.getElementById("consumptionOverTimeCanvas").getContext("2d");

  var LineChartDemo = new Chart(ctx).Line(lineChartData, {
      scaleShowVerticalLines: false,
      scaleGridLineColor: "black"
  });

  var doughnutChartData = {
    labels: [
      "Fat",
      "Protein",
      "Carbs"
  ],
  datasets: [
    {
        data: [300, 50, 100],
        fillColor: "rgba(241,119,0,0.5)",
    }]

  }
  Chart.defaults.global.responsive = true;
  Chart.defaults.global.animationEasing = "easeOutBounce";
  var ctx2 = document.getElementById("healthScoreCanvas").getContext("2d");

  var doughnutChart = new Chart(ctx2).Bar(doughnutChartData, {

  });
};

StatsController.$inject = ['$scope'];

angular.module('Gobble').controller('StatsController', StatsController)

})();
