(function() {
var FridgeListController = function($scope, fridgeItem) {
  // TODO: get data from somewhere
  $scope.list = fridgeItem.getFridge();

  $scope.OldFood = ["Manky Cheese", "Green Mould", "Smelly Fish"];

  $scope.remove = function(item) {
    var index = $scope.list.indexOf(item);
    $scope.list.splice(index, 1);
  };
};

FridgeListController.$inject = ['$scope', 'fridgeItem'];

angular.module('Gobble').controller('FridgeListController', FridgeListController)

})();

// Service to add items to the fridge
angular.module('Gobble').service('fridgeItem', function () {
  var fridge = ["Cheese", "Bacon", "Cabbage", "Milk", "Tomatoes", "Beef", "Pineapple", "Raspberries", "Eggs", "Chicken Breast", "Red Pepper", "Sweet Potatoe", "Mince", "Beetroot", "Cucumber", "White Wine"];



  return {
    fridge: function (item) {
      if(fridge.indexOf(item) < 0) {
        fridge.push(item);
      }
    },

    getFridge: function() {
      return fridge;
    }
  };
});
